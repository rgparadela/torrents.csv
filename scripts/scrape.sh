    
    
        found_line=$(rg -n $infohash $torrents_csv | cut -d : -f 1) 

    if [ ! -z $found_line ]; then
      sed -i "$found_line c$add_line" $torrents_csv
      echo -e "Found $name, updating peers"
    else
      # Append the add lines to the torrents.csv file
      echo -e "\n$add_line" >> $torrents_csv
      truncate -s -1 $torrents_csv # Removing last newline
      echo -e "Added $name"
    fi